# PDF-to-JSON convertor for Custom HuggingFace Datasets

Takes in a project directory name and converts all PDFs in the project director to text files and then converts them to JSON file format for later use with HuggingFace transformer models.
