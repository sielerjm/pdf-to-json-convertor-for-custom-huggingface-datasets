"""
Title: Helpers creation
Author: Michael Sieler
Date: June 26th, 2021
Description: This file contains all the functions needed to run the main script

How to run:
    ./PDF-to-JSON_convertor.py -p Winner_Test/PDF -t Winner_Test/Text -j Winner_Test/JSON

    ./PDF-to-JSON_convertor.py -p Corpus/Winners/Proposals/PDF -t Corpus/Winners/Proposals/Text -j Corpus/Winners/Proposals/JSON

    ./PDF-to-JSON_convertor.py -p Corpus/Winners/Personal_Statements/PDF -t Corpus/Winners/Personal_Statements/Text -j Corpus/Winners/Personal_Statements/JSON

Notes:
    - Program takes approximately 1s/pdf w/ 10 pdfs to run but becomes increasingly faster with more PDFs (e.g. ~80s/pdf w/ 90+ pdfs)

To-do:
    - [] clean_up_text() not working on some edge cases:
        - "- " to "-"
        - repeated "dddddd" not being matched in find_misc()
    - [] Add succinct comments to explain code
    - [] File naming for JSON file could be improved
"""


"""
LIBRARIES
"""


import io
import sys
import os
import getopt
import re
import time
import json


from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
from io import StringIO




"""
Functions
"""


# def main(argv):
#     # See ref. 2
#
#     # Variables
#     inputfile = ''
#     outputfile = ''
#     list_of_pdfs = ''
#
#     # Parse command line arguments
#     try:
#         opts, args = getopt.getopt(argv,"hp:t:j:",["pfile=","tfile=","jfile="])
#     except getopt.GetoptError:
#         print('PDF-to-JSON_convertor.py -p <path_to_PDF_directory> -t <path_to_text_directory> -j <path_to_json_directory>')
#         sys.exit(2)
#     for opt, arg in opts:
#         if opt == '-h' or opt == '--help':
#             print('PDF-to-JSON_convertor.py -p <path_to_PDF_directory> -t <path_to_text_directory> -j <path_to_json_directory>')
#             sys.exit()
#         elif opt in ("-p", "--pfile"):
#             input_dir = arg
#         elif opt in ("-t", "--tfile"):
#             output_dir = arg
#         elif opt in ("-j", "--jfile"):
#             json_dir = arg
#
#     # loop through project directory
#     list_of_pdf_paths = find_file_path_names(input_dir)
#     pdf_text_json_convertor(list_of_pdf_paths, input_dir, output_dir, json_dir)


# Converts PDF file to a string
def convert_pdf_to_txt(path):
    # See ref. 1

    rsrcmgr = PDFResourceManager()
    retstr = StringIO()
    codec = 'utf-8'
    laparams = LAParams()
    device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
    fp = open(path, 'rb')
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    password = ""
    maxpages = 0
    caching = True
    pagenos=set()

    for page in PDFPage.get_pages(fp, pagenos, maxpages=maxpages, password=password, caching=caching, check_extractable=True):
        interpreter.process_page(page)

    text = retstr.getvalue()

    fp.close()
    device.close()
    retstr.close()
    return text


# Creates a list of file pathnames from PDFs in proj directory
def find_file_path_names(input_dir):
    # Loop through files in directory
    # See ref. 3
    pdf_file_list = []
    # directory = os.fsencode(input_dir) # commented this out from SE answer

    for file in os.listdir(input_dir):
        filename = os.fsdecode(file)
        if filename.endswith(".pdf"):
            pdf_file_list.append(os.path.join(input_dir, filename))
            continue
        else:
            continue

    return pdf_file_list


# Iterates through each PDF in proj directory
def pdf_text_json_convertor(path, input_dir, output_dir, json_dir):
    py_obj = []
    count = 0
    max_length = 0

    for file in path:
        pdfConverter = convert_pdf_to_txt(file)
        pdfConverter = clean_up_text(pdfConverter)
        save_text_to_file(pdfConverter, count, output_dir, path)
        py_obj.append(text_to_dict(pdfConverter, path, count, input_dir))
        max_length = max_num_words(py_obj[count]["length"], max_length)
        count += 1  # counter for text file naming

    export_dict_as_json(py_obj, json_dir)
    print("Maximum number of words was: {}".format(max_length))


# function of functions for cleaning up text
def clean_up_text(text):
    text = find_ref(text)
    text = find_char(text)
    text = find_whitespace(text)
    # text = find_misc(text)  # Not working
    text = delete_ref_section(text)

    return text


# Any patterns that match are replaced accordingly
def find_char(text):
    count_fi = 0
    count_dot = 0
    text = text.replace("ﬁ", "fi")
    text = text.replace("..", ".")
    text = text.replace("ﬂ", "fl")
    text = text.replace("ﬀ", "ff")
    text = text.replace("ﬃ", "ffi")
    #text = text.replace("- ", "-")  # not working

    return text


# Finds in-text citation numbers and removes them.
def find_ref(text):
    # See ref. 4

    pattern = "(\.|\,)\d+"
    text = re.sub(pattern, '.', text)

    return text


# Removes newlines
def find_whitespace(text):
    # See ref. 4

    pattern = "\n|\r|\t"
    text = re.sub(pattern, ' ', text)
    pattern = "^[ ]{2,}"
    text = re.sub(pattern, '', text)
    pattern = "[ ]{2,}"
    text = re.sub(pattern, ' ', text)

    return text

## Not Working
# def find_misc(text):
#     pattern = "(\w)\1{2,}[^\d]"
#     text = re.sub(pattern, '', text)
#
#     return text


# Deletes text in reference section
def delete_ref_section(text):
    # See ref. 4

    pattern = "(R)(e|E)(f|F)(e|E)(r|R)(e|E)(n|N)(c|C)(e|E)(s|S)"
    text = re.split(pattern, text)[0]
    pattern = "(W)(o|O)(r|R)(k|K)(s|S)[ ](c|C)(i|I)(t|T)(e|E)(d|D)"
    text = re.split(pattern, text)[0]
    pattern = "(C)(i|I)(t|T)(a|A)(t|T)(i|I)(o|O)(n|N)(s|S)"
    text = re.split(pattern, text)[0]

    return text


# Saves string to a text file
def save_text_to_file(text, count, output_dir, path):
    # See ref. 4

    temp = re.split("\/|\.", path[count])[-2]  # extracts original file name
    file_name = os.path.join(output_dir, (temp + ".txt"))

    with open(file_name, "w") as output:
        output.write(text)


# converts text to a dict
def text_to_dict(text, path, count, input_dir):
    temp_win = False
    if ("winner" in input_dir):
        temp_win = True

    temp_dict = {"name": re.split("\/|\.", path[count])[-2], "winner": temp_win, "text": text, "length": len(text.split())}

    return temp_dict


# exports dict to a json object
def export_dict_as_json(object, json_dir):
    temp_pathname = re.split("\/|\.", json_dir)[-1]
    temp_pathname = json_dir.replace("/", "_")

    json_obj = json.dumps(object)

    with open((json_dir+"/"+temp_pathname+".json"), 'w') as fout:
        json.dump(object, fout)


# Compares num of words of current text to max_length
def max_num_words(temp_length, max_length):
    if temp_length > max_length:
        max_length = temp_length

    return max_length
