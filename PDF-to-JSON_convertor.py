#!/usr/bin/env python3

"""
Title: PDF to JSON convertor for custom HuggingFace dataset creation
Author: Michael Sieler
Date: June 26th, 2021
Description: Extracts text from PDF files and converts them to JSON format compatible for use with HuggingFace transformer models
Inputs: path to directory containing PDFs, paths for outputing text files and a JSON file.
Outputs: Converted PDF files as text files and a single JSON file containing text and some metadata

How to run:
    ./PDF-to-JSON_convertor.py -p Winner_Test/PDF -t Winner_Test/Text -j Winner_Test/JSON

    ./PDF-to-JSON_convertor.py -p Corpus/Winners/Proposals/PDF -t Corpus/Winners/Proposals/Text -j Corpus/Winners/Proposals/JSON

    ./PDF-to-JSON_convertor.py -p Corpus/Winners/Personal_Statements/PDF -t Corpus/Winners/Personal_Statements/Text -j Corpus/Winners/Personal_Statements/JSON

Notes:
    - Program takes approximately 1s/pdf w/ 10 pdfs to run but becomes increasingly faster with more PDFs (e.g. ~80s/pdf w/ 90+ pdfs)

To-do:
    - [] clean_up_text() not working on some edge cases:
        - "- " to "-"
        - repeated "dddddd" not being matched in find_misc()
    - [] Add succinct comments to explain code
    - [] File naming for JSON file could be improved
"""


"""
LIBRARIES
"""


from helpers import *


start_time = time.time()


"""
Main
"""

def main(argv):
    # See ref. 2

    # Variables
    inputfile = ''
    outputfile = ''
    list_of_pdfs = ''

    # Parse command line arguments
    try:
        opts, args = getopt.getopt(argv,"hp:t:j:",["pfile=","tfile=","jfile="])
    except getopt.GetoptError:
        print('PDF-to-JSON_convertor.py -p <path_to_PDF_directory> -t <path_to_text_directory> -j <path_to_json_directory>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h' or opt == '--help':
            print('PDF-to-JSON_convertor.py -p <path_to_PDF_directory> -t <path_to_text_directory> -j <path_to_json_directory>')
            sys.exit()
        elif opt in ("-p", "--pfile"):
            input_dir = arg
        elif opt in ("-t", "--tfile"):
            output_dir = arg
        elif opt in ("-j", "--jfile"):
            json_dir = arg

    # loop through project directory
    list_of_pdf_paths = find_file_path_names(input_dir)
    pdf_text_json_convertor(list_of_pdf_paths, input_dir, output_dir, json_dir)



# Main function
if __name__ == '__main__':
    main(sys.argv[1:])


print("--- %s seconds ---" % (time.time() - start_time))


"""
REFERENCES
"""


# 1) PDF miner and PDF to text function: https://stackoverflow.com/questions/26494211/extracting-text-from-a-pdf-file-using-pdfminer-in-python

# 2) Taking command line arguments: https://www.tutorialspoint.com/python/python_command_line_arguments.htm

# 3) Iterate through directory to get each file's path name: https://stackoverflow.com/questions/10377998/how-can-i-iterate-over-files-in-a-given-directory

# 4) Regex builder and tester: https://regexr.com/
